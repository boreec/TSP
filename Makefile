CC=g++
CXXFLAGS=-Wall -Wextra -ansi -pedantic -std=c++17 -O1 -O2
SRC_DIR=src
TARGETS=Activite_01 Activite_02

all:$(TARGETS)

Activite_02: $(SRC_DIR)/Job.o $(SRC_DIR)/Instance.o $(SRC_DIR)/InstanceParser.o $(SRC_DIR)/Activite_02.o
	$(CC) $(CXXFLAGS) -o $@ $^

Activite_01: $(SRC_DIR)/Job.o $(SRC_DIR)/Instance.o $(SRC_DIR)/InstanceParser.o $(SRC_DIR)/Activite_01.o
	$(CC) -o $@ $^

$(SRC_DIR)/Activite_02.o: $(SRC_DIR)/Activite_02.cpp
	$(CC) $(CXXFLAGS) -c $(SRC_DIR)/Activite_02.cpp  -o $@

$(SRC_DIR)/Activite_01.o: $(SRC_DIR)/Activite_01.cpp
	$(CC) $(CXXFLAGS) -c $(SRC_DIR)/Activite_01.cpp  -o $@

$(SRC_DIR)/InstanceParser.o: $(SRC_DIR)/InstanceParser.cpp
	$(CC) $(CXXFLAGS) -c $^ -o $@

$(SRC_DIR)/Instance.o: $(SRC_DIR)/Instance.cpp
	$(CC) $(CXXFLAGS) -c $^ -o $@

$(SRC_DIR)/Job.o: $(SRC_DIR)/Job.cpp
	$(CC) $(CXXFLAGS) -c $^ -o $@

.PHONY: clean cleanall all

clean:
	rm -f $(SRC_DIR)/*.o

cleanall: clean
	rm -f $(TARGETS)
