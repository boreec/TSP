#!/bin/bash
declare -a inits=("RND" "EDD" "MDD")
declare -a selects=("FIRST" "BEST")
declare -a neighbours=("SWAP" "EXCHANGE" "INSERT")
data_file="data/wt100.txt"
output_dir="result"
executable="Activite_02"
total_commands=`expr ${#inits[@]} "*" ${#selects[@]} "*" ${#neighbours[@]}`
command_counter=0

make cleanall
make -j8
rm -rf ${output_dir}
mkdir -p ${output_dir}

for i in "${inits[@]}"
do
    for j in "${selects[@]}"
    do
	for k in "${neighbours[@]}"
	do
	    ((command_counter=command_counter+1))
	    command="./${executable} -d ${data_file} -i ${i} -s ${j} -n ${k}"
	    output_file="${output_dir}/${i}_${j}_${k}.txt"
	    touch ${output_file}
	    printf "${command_counter}/${total_commands}: ${command}"
	    printf "$(time ${command} > ${output_file})\n"
	done
    done
done
