#ifndef __INSTANCE_HPP__
#define __INSTANCE_HPP__
#include <algorithm>
#include <cassert>
#include <iostream>
#include <random>
#include <string>
#include <vector>
#include "Job.hpp"

enum class Init {RND, EDD, MDD};
enum class Select{FIRST, BEST};
enum class Neighbours{SWAP, INSERT, EXCHANGE};

class Instance{
private:
  std::vector<Job> jobs;
public:
  Instance(std::vector<Job>);
  unsigned scheduling_cost() const;

  /*
    Compute a new instance after scheduling the jobs in a random order.
  */
  static Instance schedule_randomly(Instance i);

  static Instance schedule_by_EDD(Instance i);

  static Instance schedule_by_MDD(Instance i);

  /* Swap two jobs */
  static std::vector<Job> swap(const std::vector<Job>, unsigned int, unsigned int);

  /* Reinsert a job to a new position */
  static std::vector<Job> insert(const std::vector<Job>, unsigned int, unsigned int);

  /* Swap two consecutive jobs */
  static std::vector<Job> exchange(const std::vector<Job>, unsigned int);
  
  static std::vector<Job> recursive_hill_climbing(Select, Neighbours, std::vector<Job>);
  static std::vector<Job> iterative_hill_climbing(const Select&, Neighbours , std::vector<Job>);
  /*
   * Analyze the content of two instances and return true if both of them
   * share the same jobs (with the same values, not necessarily the same addresses).
   * The jobs order is has no impact on that comparison.
   * */
  static bool share_jobs(Instance i1, Instance i2);
  std::vector<Job> get_jobs() const;
  std::string to_string() const;

  static Neighbours next_neighbour(Neighbours current_neighbour);
};
#endif
