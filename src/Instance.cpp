#include "Instance.hpp"

Instance::Instance(std::vector<Job> j): jobs(j){

}

unsigned int Instance::scheduling_cost() const {
  unsigned int elapsed_time = 0;
  unsigned int cost = 0;
  for(unsigned int i = 0; i < jobs.size(); i++){
    elapsed_time += jobs[i].get_process_time();
    cost += jobs[i].get_weight() * std::max((int)(elapsed_time - jobs[i].get_due_time()), 0);
  }
  return cost;
}

Instance Instance::schedule_randomly(Instance i){
  std::vector<Job> j = i.get_jobs();
  auto rng = std::default_random_engine{};
  std::shuffle(std::begin(j), std::end(j), rng);
  return Instance(j);
}

Instance Instance::schedule_by_EDD(Instance i){
  std::vector<Job> j = i.get_jobs();
  std::sort(j.begin(), j.end(),
	    [](Job j1, Job j2){
	      return j1.get_due_time() < j2.get_due_time();
	    });
  return Instance(j);
}

Instance Instance::schedule_by_MDD(Instance i){
  std::vector<Job> unsorted_tasks = i.get_jobs();
  std::vector<Job> sorted_tasks;
  int elapsed_time = 0;
  while(unsorted_tasks.size() > 0){
    Job best_job = unsorted_tasks[0];
    int best_mdd = std::max((int)(elapsed_time + best_job.get_process_time()), (int)best_job.get_due_time());
    for(unsigned int t = 0; t < unsorted_tasks.size(); t++){
      Job job = unsorted_tasks[t];
      int mdd = std::max((int)(elapsed_time + job.get_process_time()), (int)job.get_due_time());
      if(mdd < best_mdd){
	best_job = job;
	best_mdd = mdd;
      }
    }
    elapsed_time += best_job.get_process_time();
    sorted_tasks.push_back(best_job);
    unsorted_tasks.erase(std::remove(unsorted_tasks.begin(), unsorted_tasks.end(), best_job), unsorted_tasks.end());
  }
  return Instance(sorted_tasks);
}

Neighbours Instance::next_neighbour(Neighbours n){
  if(n == Neighbours::SWAP)
    return Neighbours::EXCHANGE;
  if(n == Neighbours::INSERT)
    return Neighbours::SWAP;
  // if n == EXCHANGE
  return Neighbours::INSERT;
}

std::vector<Job> Instance::iterative_hill_climbing(const Select& s, Neighbours n, std::vector<Job> j){
  int initial_optimum = Instance(j).scheduling_cost();
  int best_optimum = initial_optimum;
  std::vector<Job> best_solution = j;
  bool better_optimum_found = true;
  
  while(better_optimum_found){
    better_optimum_found = false;
    for(unsigned int x = 0; x < j.size(); ++x){
      for(unsigned int y = 0; y < j.size(); ++y){
	std::vector<Job> next_solution;
	int next_optimum;
	switch(n){
	case Neighbours::SWAP:
	  next_solution = Instance::swap(j, x, y);
	  break;
	case Neighbours::EXCHANGE:
	  next_solution = Instance::exchange(j, y);
	  break;
	case Neighbours::INSERT:
	  next_solution = Instance::insert(j, x, y);
	  break;
	default:
	  std::cerr << "unknown behaviour for unknown neighborhood aproach." << std::endl;
	  std::abort();
	}
	//assert(Instance::share_jobs(next_solution, j)); //make sure no jobs are lost in the process of finding another solution (slower).
	next_optimum = Instance(next_solution).scheduling_cost();
	n = Instance::next_neighbour(n);
	if(next_optimum < best_optimum){
	  best_solution = next_solution;
	  best_optimum = next_optimum;
	  better_optimum_found = true;
	  if(s == Select::FIRST)
	    break;
	}
      }
      if(better_optimum_found && s == Select::FIRST)
	break;
      
    }
  }
  return best_solution;
}

std::vector<Job> Instance::recursive_hill_climbing(Select s, Neighbours n, std::vector<Job> j){
  int best_optima = Instance(j).scheduling_cost();
  int initial_optima = best_optima;
  std::vector<Job> best_solution = j;

  for(unsigned int x = 0; x < j.size(); ++x){
    for(unsigned int y = 0; y < j.size(); ++y){
      std::vector<Job> next_solution;
      int next_optima;
      switch(n){
      case Neighbours::SWAP:
	next_solution = Instance::swap(j, x, y);
	break;
      case Neighbours::EXCHANGE:
	next_solution = Instance::exchange(j, y);
	break;
      case Neighbours::INSERT:
	next_solution = Instance::insert(j, x, y);
	break;
      default:
	std::cerr << "unknown behaviour for unknown neighborhood aproach." << std::endl;
	std::abort();
      }
      //assert(Instance::share_jobs(next_solution, j)); //make sure no jobs are lost in the process of finding another solution (slower).
      next_optima = Instance(next_solution).scheduling_cost();
      if(next_optima < best_optima){
	best_solution = next_solution;
	best_optima = next_optima;
	if(s == Select::FIRST)
	  break;
      }
    }
    if(initial_optima != best_optima && s == Select::FIRST)
      break;
  }

  if(initial_optima == best_optima)
    return best_solution;
  return Instance::recursive_hill_climbing(s, n, best_solution);
}


std::vector<Job> Instance::swap(const std::vector<Job> j, unsigned int i, unsigned int k){
  std::vector<Job> res = j;
  if(i < j.size() && k < j.size()){
    Job tmp = res[k];
    res[k] = res[i];
    res[i] = tmp;
  }
  return res;
}

/*
 * Creates a vector of jobs from a given one by reinserting a Job i at position k.
 * If positions are out of vector's size, a copy of the initial vector is returned.
 * */
std::vector<Job> Instance::insert(const std::vector<Job> j, unsigned int i, unsigned int k){

  std::vector<Job> res = j;

  if(i >= j.size() || k >= j.size())
    return res;

  Job tmp = res[i];
  
  if(i < k){
    res.insert(res.begin() + k, tmp);
    res.erase(res.begin() + i);
  }
  else if(i > k){
    res.erase(res.begin() + i);
    res.insert(res.begin() + k, tmp);
  }
  return res;
}

std::vector<Job> Instance::exchange(const std::vector<Job> j, unsigned int i){
  return Instance::swap(j, i, i-1);
}

std::vector<Job> Instance::get_jobs() const{
  return jobs;
}

std::string Instance::to_string() const{
  std::string result = "";
  result += jobs[0].to_string();
  for(unsigned int i = 1; i < jobs.size(); i++){
    result += " " + jobs[i].to_string();
  }
  return result;
}

bool Instance::share_jobs(Instance i1, Instance i2){
  std::vector<Job> jobs1 = i1.get_jobs();
  std::vector<Job> jobs2 = i2.get_jobs();
  bool share = jobs1.size() == jobs2.size();
  unsigned int i = 0, j = 0;
  while(share && i < jobs1.size()){
    bool found = false;
    while(!found && j < jobs2.size()){
      if(jobs1[i] == jobs2[j]){
	found = true;
      }
      ++j;
    }
    share=found;
    i++;
    j=0;
  }
  return share;
}
