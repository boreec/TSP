#include "Job.hpp"

Job::Job(unsigned int p, unsigned int w, unsigned int d) : process_time(p), weight(w), due_time(d){

}

unsigned int Job::get_process_time() const{
  return process_time;
}

unsigned int Job::get_due_time() const{
  return due_time;
}

unsigned int Job::get_weight() const{
  return weight;
}

std::string Job::to_string() const{
  return "J(" + std::to_string(process_time) + "," + std::to_string(weight) + "," + std::to_string(due_time) + ")";
}

bool Job::operator==(const Job& j) const{
  return process_time == j.get_process_time() && weight == j.get_weight() && due_time == j.get_due_time();
}
