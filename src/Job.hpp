#ifndef __JOB_HPP__
#define __JOB_HPP__
#include <string>
class Job{
private:
  unsigned int process_time;
  unsigned int weight;
  unsigned int due_time;
public:
  Job(unsigned int p, unsigned int d, unsigned int w);
  unsigned int get_process_time() const;
  unsigned int get_due_time() const;
  unsigned int get_weight() const;
  std::string to_string() const;
  bool operator==(const Job&) const;
};
#endif
