#include <iostream>
#include <vector>
#include "InstanceParser.hpp"
#include "Instance.hpp"

void print_instances_cost(std::vector<Instance> instances){
  for(unsigned int i = 0; i < instances.size(); i++){
    unsigned int scheduling_cost = instances[i].scheduling_cost();
    std::cout << "[" << i << "] cost: " << scheduling_cost << std::endl;
  }
}

int main(int argc, char *argv[]){
  if(argc < 2){
    std::cerr << "data file expected" << std::endl;
    return -1;
  }
  std::vector<Instance> instances;
  instances = InstanceParser::parse(argv[1]);
  for(unsigned int i = 0; i < instances.size(); i++){
      instances[i] = Instance::schedule_by_MDD(instances[i]);
  }
  print_instances_cost(instances);
  return 0;
}
