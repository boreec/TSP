#ifndef __INSTANCE_PARSER_HPP__
#define __INSTANCE_PARSER_HPP__
#include <fstream>
#include <iostream>
#include <regex>
#include <string>
#include <vector>
#include "Instance.hpp"

class InstanceParser{
  static const unsigned int INSTANCE_QUANTITY = 125;
public:
  static std::vector<Instance> parse(std::string);
};
#endif
