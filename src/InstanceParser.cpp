#include "InstanceParser.hpp"

std::vector<Instance> InstanceParser::parse(std::string filename){
  std::vector<Instance> result;
  std::ifstream f;
  f.open(filename.c_str());
  if(f.is_open()){
    std::string line;
    std::vector<std::string> words;
    std::vector<int> values;
    std::regex we_re("\\s+");
    while(std::getline(f, line)){
	words = {std::sregex_token_iterator(line.begin(), line.end(), we_re, -1), std::sregex_token_iterator()};
	for(unsigned int i = 1; i < words.size(); i++){
	  values.push_back(std::stoi(words[i]));
	}
    }
    const unsigned int problem_size = values.size() / (3 * INSTANCE_QUANTITY);
    for(unsigned int j = 0; j < INSTANCE_QUANTITY; j++){
      std::vector<Job> jobs;
      for(unsigned int k = 0; k < problem_size; k++){
	int p = values[j * problem_size * 3 + k];
	int w = values[j * problem_size * 3 + problem_size + k];
	int d = values[j * problem_size * 3 + problem_size * 2 + k];
	jobs.push_back(Job(p, w, d));
      }
      result.push_back(Instance(jobs));
    }
    return result;
  }else{
    std::cerr << "can't open " << filename << std::endl;
  }
  return result;
}
