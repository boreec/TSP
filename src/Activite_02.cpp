#include <chrono>
#include <cstdlib>
#include <fstream>
#include <getopt.h>
#include <iostream>
#include <thread>
#include <vector>
#include "InstanceParser.hpp"
#include "Instance.hpp"

std::vector<unsigned> run(std::vector<Instance> instances, Init init, Select s, Neighbours n, bool output_details=false){
  int total_duration = 0;
  std::vector<unsigned> scores;
  for(unsigned int i = 0; i < instances.size(); i++){
    if(init == Init::RND){
      instances[i] = Instance::schedule_randomly(instances[i]);
    }
    else if(init == Init::EDD){
      instances[i] = Instance::schedule_by_EDD(instances[i]);
    }
    else if(init == Init::MDD){
      instances[i] = Instance::schedule_by_MDD(instances[i]);
    }
    else{
      std::cerr << "Unknown init option, aborting..." << std::endl;
      std::abort();
    }
    std::chrono::steady_clock::time_point time_begin = std::chrono::steady_clock::now();
    std::vector<Job> j = Instance::iterative_hill_climbing(s, n, instances[i].get_jobs());
    //std::vector<Job> j = Instance::recursive_hill_climbing(s, n, instances[i].get_jobs());

    std::chrono::steady_clock::time_point time_end = std::chrono::steady_clock::now();
    scores.push_back(Instance(j).scheduling_cost());
    if(output_details){
      auto time_duration = std::chrono::duration_cast<std::chrono::milliseconds>(time_end - time_begin).count();
      total_duration += time_duration;
      std::cout << "instance[" << i << "]: " << time_duration << "[ms], result: " << Instance(j).scheduling_cost() << std::endl;
    }else{
      std::cout << Instance(j).scheduling_cost() << std::endl;
    }
  }
  if(output_details){
    std::cout << "total time: " << total_duration << "ms|" << total_duration/1000 << "s" <<std::endl;
    std::cout << "average time: " << (total_duration / 125) << "ms" << std::endl;
  }
  return scores;
}

void print_help(){
  std::cout << "-h : print help (this message)" << std::endl;
  std::cout << "-d data_file : data file to use" << std::endl;
  std::cout << "-i {RND|EDD|MDD} : initial solution" << std::endl;
  std::cout << "-s {BEST|FIRST} : select improvement" << std::endl;
  std::cout << "-n {INSERT|EXCHANGE|SWAP} : choose neighbours" << std::endl;
}

std::vector<unsigned> parseExtraFile(std::string filename){
  std::vector<unsigned> v;
  std::ifstream extra_file(filename);
  if(!extra_file.is_open()){
    std::cout << "error opening file " << filename << std::endl;
  }else{
    std::string line;
    while(getline(extra_file, line)){
      if(line.back() == '\n'){
	line.pop_back();
      }
      unsigned tmp = std::stoul(line);
      std::cout << line << ":" << tmp << std::endl;
      v.push_back(std::stoul(line));
    }
  }
  return v;
}


int main(int argc, char* argv[]){
  const char* const short_opts = "d:i:s:n:h";
  std::string data_file = "";
  std::string init = "";
  std::string select = "";
  std::string neighbours = "";
  const option long_opts[] = {
      {"data", required_argument, nullptr, 'd'},
      {"init", required_argument, nullptr, 'i'},
      {"select", required_argument, nullptr, 's'},
      {"neighbours", required_argument, nullptr, 'n'},
      {"help", no_argument, nullptr, 'h'},
      {nullptr, no_argument, nullptr, 0}
  };

  if(argc < 8){
    std::cout << "arguments required -d/-i/-s/-n" << std::endl;
    print_help();
    return -1;
  }
  std::vector<unsigned> best_known_values;
  if(argc == 10){
    std::cout << "best known instances provided!" << std::endl;
    best_known_values = parseExtraFile(argv[9]);
  }
  while (true){
      const auto opt = getopt_long(argc, argv, short_opts, long_opts, nullptr);

      if(opt == -1)
	break;

      switch(opt){
        case 'd':
	  data_file = optarg;
	  break;
	case 'i':
	  init = optarg;
	  break;
	case 's':
	  select = optarg;
	  break;
	case 'n':
	  neighbours = optarg;
	  break;
        case 'h': // -h or –help
	  print_help();
	  break;
      case '?': // Unrecognized option
        default:
	  std::cerr << "unrecognized option " << optarg << std::endl;
	  break;
        }
    }

  Init i;
  Select s;
  Neighbours n;

  if(init == "RND")
    i = Init::RND;
  else if(init == "EDD")
    i = Init::EDD;
  else if(init == "MDD")
    i = Init::MDD;
  else{
    std::cerr << "Init value must be in {RND|EDD|MDD}" << std::endl;
    return -1;
  }

  if(select == "FIRST")
    s = Select::FIRST;
  else if (select == "BEST")
    s = Select::BEST;
  else{
    std::cerr << "Select value must be in {FIRST|BEST}" << std::endl;
    return -1;
  }

  if(neighbours == "SWAP")
    n = Neighbours::SWAP;
  else if(neighbours == "INSERT")
    n = Neighbours::INSERT;
  else if(neighbours == "EXCHANGE")
    n = Neighbours::EXCHANGE;
  else{
    std::cerr << "Neighbours value must be in {SWAP|INSERT|EXCHANGE}" << std::endl;
    return -1;
  }
  std::vector<Instance> instances;
  instances = InstanceParser::parse(data_file);
  std::vector<unsigned> scores = run(instances, i, s, n, true);

  if(best_known_values.size()){
  double instances_solved = 0;
  double sum_deviation = 0;
  for(unsigned i = 0; i < scores.size(); ++i){
    if(scores[i] == best_known_values[i]){
      instances_solved++;
    }
    double denominator = best_known_values[i];
    if(best_known_values[i] == 0)
      denominator = 1;
    double deviation = 100 * (scores[i] - best_known_values[i])/(denominator);
    sum_deviation += deviation;
  }
  
  std::cout << "instances solved: " << ((double)(instances_solved/scores.size())) * 100 << std::endl;
  std::cout << "mean deviation: " << (sum_deviation/(double)scores.size()) << std::endl;
  }
  return 0;
}
